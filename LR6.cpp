// LR6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;



const int N = 4; // Размерность входных данных для хеширования
bool flag = true; // Флаг работы потоков

void mythread(int s)
{
	// s - инициализация генератора псевдослучайных чисел
	srand(s);
	std::array<unsigned char, N> data_array;
	std::thread::id id = this_thread::get_id();
	while (flag)
	{
		//char Out[N * 2 + 1]; // Каждый символ в шестнадцатиричном коде 2 цифры и строка заканчивается 0
		ostringstream sout;
		sout << setfill('0')<<hex;
		for (int i = 0; i < N; i++) {
			data_array[i] = rand();
			sout << setw(2) << (int)data_array[i];
		}
		string hash = picosha2::hash256_hex_string(data_array);

		BOOST_LOG_TRIVIAL(trace) << " A trace severity message: "<<id<<" data: "<<sout.str()<<" hesh="<<hash;//логирование
		if (hash.compare(60, 4, "0000") == 0) // Нашли искомый хеш
		{
			BOOST_LOG_TRIVIAL(info) << " A info severity message: " << id << " data: " << sout.str() << " hesh=" << hash;
			cout << "Hash found \n";
			system("pause");
		}
	}
}

int main(int argc, char *argv[])
{
	int m = thread::hardware_concurrency();//количество потоков
    std::cout << "number of threads   m="<<m<<"\n"; 
	if (argc > 1)
	{
		sscanf_s(argv[1], "%d", &m);
	}
//	boost::log::add_console_log(std::cout, keywords::format = "[%TimeStamp%]: %Message%");
	logging::add_file_log // расширенная настройка
	(
		keywords::file_name = "log_%N.log",
		keywords::rotation_size = 10 * 1024 * 1024,
		keywords::time_based_rotation = sinks::file::rotation_at_time_point{ 0, 0, 0 },
		keywords::format = "[%TimeStamp%]: %Message%"
	);
	logging::core::get()->set_filter
	(
		logging::trivial::severity <= logging::trivial::info
	);
	// Создаем потоки динамически
	thread **pTh = new thread*[m];
	for (int i = 0; i < m; i++) pTh[i] = new thread(mythread, i + 1);
	cout << "Thread Ok!!!!!\n";
	cout << "For exit, press Enter!\n";
	cin.get();
	flag = false;
	for (int i = 0; i < m; i++) pTh[i]->join();
	delete[] pTh;
	cout << "Thread is end!\n";


	
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
